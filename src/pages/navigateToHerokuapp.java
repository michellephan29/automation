package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class NavigateToHerokuapp {
	
	WebDriver driver;
	
	public NavigateToHerokuapp (WebDriver driver) {
		this.driver = driver;
	}
	
	/**
	 * This POM method, is used in the test case to 
	 * Click on the java script alert link on the page
	 * @param driver
	 */

	public void navigateToJavaScriptAlerts(WebDriver driver) {

		WebElement javaScriptAlertLink = driver.findElement(By.cssSelector("#content > ul > li:nth-child(29) > a"));

		javaScriptAlertLink.click();
	}
}
