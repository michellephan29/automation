package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class JavaScriptAlertPage {
	
	WebDriver driver;
	
	
	public JavaScriptAlertPage(WebDriver driver) {
		
		this.driver = driver;
		
	}
	
	/**
	 * This POM method is used in the test case to click on the
	 * Alerts button on the page
	 */
	
	public void clickOnAlerts() {
		
		WebElement jsConfirmButton = driver.findElement(By.cssSelector("#content > div > ul > li:nth-child(2) > button"));
		
		jsConfirmButton.click();
		driver.switchTo().alert().accept();
		
	}
	
	/**
	 * This POM method used in the test case 
	 * go validate the result text message
	 * @return
	 */
	
	public String getResultElement() {
		
		WebElement resultElement = driver.findElement(By.xpath("//*[@id=\"result\"]"));
		
		return resultElement.getText();
		 
	}
}	
