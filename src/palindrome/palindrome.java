package palindrome;

public class Palindrome {
	
	/**
	 isPalindrome function validates if a String is a Palindrome or not
	 * @param string
	 * @return boolean
	 */
	
    public boolean isPalindrome(String string){
    	
    	//change the string to lower case, so the function is not case sensitive
    	String beginningArray = string.toLowerCase();
    	
    	int endArray = string.length()-1;
    	
    	char[] arrChars = beginningArray.toCharArray();
    	
    	//loops through the string
    	//if the first char and the last char are not equal, then return false
    	//if they are equal, then the counter is incremented until all chars are checked
    	 for(int i=0; i < beginningArray.length(); i++) {
    	        if(arrChars[i] != arrChars[endArray - i]) {
    	            return false;
    	        }
    	    }
    	return true;
    }
}
